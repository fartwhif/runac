﻿using Microsoft.Win32;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Data;

namespace RunAC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Config config = new Config();
        public MainWindow()
        {
            InitializeComponent();
            config.Load();
            DataContext = config;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (config.ServerType != ServerType.ACE)
            {
                MessageBox.Show("GDLE pattern not yet implemented.");
                return;
            }
            Thread thread = new Thread(() =>
            {
                try
                {
                    string arguments = $"-a {config.User} -v {config.Pass} -h {config.Server}";
                    if (StartProc(config.AcclientPath, arguments, config.Inject) == -1)
                    {
                        MessageBox.Show("Unable to inject decal", "error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            });
            thread.Start();
        }
        private static string _InjectDllPath = null;
        private static string InjectDllPath
        {
            get
            {
                if (_InjectDllPath != null)
                {
                    return _InjectDllPath;
                }
                try
                {
                    using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"Software\Wow6432Node\Decal\Agent"))
                    {
                        if (key != null)
                        {
                            object o = key.GetValue("AgentPath");
                            if (o != null)
                            {
                                string decalProgramPath = o as string;
                                if (!string.IsNullOrEmpty(decalProgramPath))
                                {
                                    string injectPath = Path.Combine(decalProgramPath, "Inject.dll");
                                    if (File.Exists(injectPath))
                                    {
                                        _InjectDllPath = injectPath;
                                        return injectPath;
                                    }
                                    else
                                    {
                                        _InjectDllPath = "";
                                    }
                                }
                                else
                                {
                                    _InjectDllPath = "";
                                }
                            }
                            else
                            {
                                _InjectDllPath = "";
                            }
                        }
                        else
                        {
                            _InjectDllPath = "";
                        }
                    }
                }
                catch (Exception) { _InjectDllPath = ""; }
                return null;
            }
        }
        private static int StartProc(string exeFilPath, string arguments, bool decalInject)
        {
            if (decalInject)
            {
                string injectDll = InjectDllPath;
                if (string.IsNullOrEmpty(injectDll))
                {
                    return -1;
                }
                else
                {
                    int pid = -1;
                    if (Injector.RunSuspendedCommaInjectCommaAndResume(exeFilPath, arguments, out pid, injectDll, "DecalStartup"))
                    {
                        return pid;
                    }
                    else
                    {
                        return -1;
                    }
                }
            }
            else
            {
                ProcessStartInfo psi = new ProcessStartInfo
                {
                    FileName = exeFilPath,
                    Arguments = arguments,
                    WorkingDirectory = Path.GetDirectoryName(exeFilPath)
                };
                Process newProc = Process.Start(psi);
                return newProc.Id;
            }
        }
    }



    public enum ServerType
    {
        GDLE,
        ACE
    }
    public class Config : INotifyPropertyChanged
    {
        public Config() { }
        private bool Loading = false;
        public void Load()
        {
            try
            {
                Loading = true;
                string cfgLoc = cfg_location();
                if (!File.Exists(cfgLoc))
                {
                    File.Create(cfgLoc);
                    if (File.Exists(defaultACClientPath))
                    {
                        AcclientPath = defaultACClientPath;
                    }
                }
                else
                {
                    string[] arLines = File.ReadAllLines(cfgLoc);
                    AcclientPath = arLines[0];
                    Server = arLines[1];
                    User = arLines[2];
                    Pass = arLines[3];
                    ServerType = (ServerType)int.Parse(arLines[4]);
                    Inject = bool.Parse(arLines[5]);
                }
            }
            catch (Exception ex) { }
            Loading = false;
        }
        private string cfg_location()
        {
            var eaLoc = Assembly.GetExecutingAssembly().Location;
            var cfgLoc = Path.Combine(Path.GetDirectoryName(eaLoc), Path.GetFileName(eaLoc) + ".cfg");
            return cfgLoc;
        }
        public void Save()
        {
            try
            {
                string cfgLoc = cfg_location();
                List<string> data = new List<string>
                {
                    AcclientPath,
                    Server,
                    User,
                    Pass,
                    ((int)ServerType).ToString(),
                    Inject.ToString(),
                };
                File.WriteAllLines(cfgLoc, data);
            }
            catch (Exception ex) { }
        }
        private const string defaultACClientPath = "C:\\Turbine\\Asheron's Call\\acclient.exe";
        private string _AcclientPath { get; set; }
        private string _Server { get; set; }
        private ServerType _ServerType { get; set; }
        private string _User { get; set; }
        private string _Pass { get; set; }
        private bool _Inject { get; set; }

        public ServerType ServerType
        {
            get { return _ServerType; }
            set
            {
                _ServerType = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("ServerType"));
                }
                if (!Loading)
                {
                    Save();
                }
            }
        }
        public string AcclientPath
        {
            get { return _AcclientPath; }
            set
            {
                _AcclientPath = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("AcclientPath"));
                }
                if (!Loading)
                {
                    Save();
                }
            }
        }
        public string Server
        {
            get { return _Server; }
            set
            {
                _Server = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Server"));
                }
                if (!Loading)
                {
                    Save();
                }
            }
        }
        public string User
        {
            get { return _User; }
            set
            {
                _User = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("User"));
                }
                if (!Loading)
                {
                    Save();
                }
            }
        }
        public string Pass
        {
            get { return _Pass; }
            set
            {
                _Pass = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Pass"));
                }
                if (!Loading)
                {
                    Save();
                }
            }
        }
        public bool Inject
        {
            get { return _Inject; }
            set
            {
                _Inject = value;
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Inject"));
                }
                if (!Loading)
                {
                    Save();
                }
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;
    }
    public class RadioBoolToIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int integer = (int)value;
            if (integer == int.Parse(parameter.ToString()))
                return true;
            else
                return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return parameter;
        }
    }
}
